
// Controller functions here... SEe on Javascripti fail

function loadCompanies() {
    fetchCompanies().then(
        function(companies){  // instruktsioon then-meetodile

//  console.log(companies) - näitab Consoles tulemust
// document.getElementById("comapaniesList").innerHTML = companyHtml
            
            let companyHtml = "";
            
            for(let i = 0; i < companies.length; i++) {
                companyHtml = companyHtml + `
                    <tr>
                    <td>${companies[i].id}</td>
                    <td>${companies[i].name}</td>
                    <td><img src="${companies[i].logo}" height="40"></td>
                    <td>
                    <button type="button" class="btn btn-outline-danger" onclick="handleDeleteButtonClick(${companies[i].id})">Kustuta</button>
                    <button type="button" class="btn btn-info" onclick="handleEditButtonClick(${companies[i].id})">Muuda</button>
                    </tr>
                
                `;
            }
            document.getElementById("companiesList").innerHTML = companyHtml;

            
/*
                <b>${companies[i].name}</b><br>
                <img src="${companies[i].logo}"><hr>
----------------------------------------------------------------

let companiesHtml = "";
            for(let i = 0; i < companies.length; i++) {
                companiesHtml = companiesHtml + `
                    <tr>
                        <th scope="row">${companies[i].id}</th>
                        <td>${companies[i].name}</td>
                        <td><img src="${companies[i].logo}" height="20"></td>
                    </tr>
                `;
            }
            document.getElementById("companiesList").innerHTML = companiesHtml;

*/

        }
    );
}

function handleSave() {
    if(isFormValid()===false){
//  if(!isFormValid()){  - teeb sama
        return;
    }
    if (document.getElementById("id").value > 0) {
        // Edit
        handleEdit();
    } else {
        // Add
        handleAdd();
    }
}

function handleEdit() {
    let company = {
        id: document.getElementById("id").value,
        name: document.getElementById("name").value,
        logo: document.getElementById("logo").value
    };
    putCompany(company).then(
        function() {
            loadCompanies();
            $("#companyModal").modal("hide");
        }
    );
}

function handleAddButtonClick() {
    $("#companyModal").modal("show");
    document.getElementById("id").value = null;
    document.getElementById("name").value = null;
    document.getElementById("logo").value = null;
}

function handleAdd() {
    let company = {
        name: document.getElementById("name").value,
        logo: document.getElementById("logo").value
    };
    postCompany(company).then(
        function() {
            loadCompanies();
            $("#companyModal").modal("hide");
        }
    );
}




function handleEditButtonClick(id) {
    $("#companyModal").modal("show");
    fetchCompany(id).then(
        function(company) {
            document.getElementById("id").value = company.id;
            document.getElementById("name").value = company.name;
            document.getElementById("logo").value = company.logo;
        }
    )
}


function handleDeleteButtonClick(id){
    if(confirm("Oled Sa ikka kindel, et soovid selle ettevõtte kustutada?")){
        deleteCompany(id).then(loadCompanies);
    }
}


function isFormValid(){
    let name = document.getElementById("name").value;
    let logo = document.getElementById("logo").value;
    if(name === null || name.length < 1){
        document.getElementById("errorMessage").innerText = "Ettevõtte nimi on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    if(logo === null || logo.length < 1){
        document.getElementById("errorMessage").innerText = "Logo lisamine on kohustuslik!";
        document.getElementById("errorMessage").style.display = "block";
        return false;
    }
    return true;
}




function loadSomething() {
    fetchSomething().then(displaySomething);
}
